# TD 1 - Printing records according to a filter

The objective of the td1 is to discover the componant *selectInput* and *tableOutput* in order to filter records according to a modality selected by the user, and print the resulting table.

### Results

![]() <img src="images/td1.png"  width="500">